# MemeBot
> This is a facebook chatbot that manage your subscriptions to your loved meme fan pages

## Notice
1. Copy `.env.sample` into `.env` and fill in your own credentials
- Run `cp .env.sample .env` in your terminal.


## Development
> It will run `[nodemon](https://github.com/remy/nodemon)` and recompile when src/ changes
- `npm run dev`

## Test
> We use `[ava](https://github.com/avajs/ava)` as test framework.
- `npm run test`

## Build and Production
- `npm run build`
- `npm run start`

## Contributing
1. Fork it
2. Create your feature branch (git checkout -b my-feature)
3. Commit your changes (git commit -am 'Added feature')
4. Push to the branch (git push origin my-feature)
5. Create new Pull Request


## License
- MIT
