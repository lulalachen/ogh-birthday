import fs from 'fs'
if (fs.existsSync('./.env')) {
  console.log('Loading env from .env')
  require('dotenv').config()
}

export const VERIFY_TOKEN = process.env.VERIFY_TOKEN
export const POST_ID = process.env.POST_ID
export const PAGE_ACCESS_TOKEN = process.env.PAGE_ACCESS_TOKEN
