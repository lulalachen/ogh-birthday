import { Router } from 'express'
import webhookVerification from './webhookVerification'
import handleWebhookEvent from './handleWebhookEvent'
import logMessageMiddleware from './logMessageMiddleware'

const router = Router()

router.get('/', (req, res) => {
  res.send({ message: 'alive' })
})

router.get('/webhook', webhookVerification)
router.post('/webhook', logMessageMiddleware, handleWebhookEvent)

export default router
