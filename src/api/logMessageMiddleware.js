import chalk from 'chalk'

const logMessageMiddleware = (req, res, next) => {
  const data = req.body
  if (data.object === 'page') {
    const { entry: entries } = data
    entries.forEach((entry) => {
      console.log(chalk.bold('[Page Message]'))
      console.log(JSON.stringify(entry, '  ', 2))
    })
    next()
  }
}

export default logMessageMiddleware
