import R from 'ramda'
import FB from 'fb'
import { POST_ID } from '../config'

const photoUrls = [
  'https://imgur.com/GSLH5mH.jpg',
  'https://imgur.com/TTa19sb.jpg',
  'https://imgur.com/OHvTK9N.jpg',
  'https://imgur.com/pB90BFW.jpg',
  'https://imgur.com/rIaEOMR.jpg',
  'https://imgur.com/60iUEHO.jpg',
  'https://imgur.com/TgOKzpW.jpg',
  'https://imgur.com/X0VyMBH.jpg',
]

const getRandomIndexFrom =
  number => Math.floor(Math.random() * (number))

const getPhotoUrl = () =>
  photoUrls[getRandomIndexFrom(photoUrls.length)]

const parseIntoComments = R.pipe(
  R.pluck('value'),
  R.map(
    R.pick(['comment_id', 'sender_name', 'message', 'post_id', 'item']),
  ),
)

const privateReply = (commentId, senderName) =>
  new Promise((resolve, reject) => FB.api(
    `${commentId}/private_replies/`,
    'post',
    {
      message: `謝謝${senderName}~
廣涵愛你 <3
要記得親自敲廣涵跟他說聲生日快樂唷~~~`,
    },
    ({ data, error }) => {
      if (error) {
        reject(error)
      }
      console.log(`private reply to ${senderName}`)
      resolve(data)
    },
))

const commentReply = (commentId, senderName, photoUrl) =>
  new Promise((resolve, reject) => FB.api(
    `${commentId}/comments`,
    'post',
    {
      message: `謝謝 ${senderName}
廣涵最愛你了 <3`,
      attachment_url: photoUrl,
    },
    ({ data, error }) => {
      if (error) {
        reject(error)
      }
      console.log(`reply to ${senderName}`)
      resolve(data)
    },
))

const handleWebhookEvent = ({ body: { object, entry } }, res) => {
  const { changes } = entry[0]

  if (object === 'page') {
    const comments = parseIntoComments(changes)
    comments.map(({
      // message,
      item,
      post_id: postId,
      sender_name: senderName,
      comment_id: commentId,
    }) => {
      if (postId === POST_ID && senderName !== '噢! 廣涵' && item === 'comment') {
        const photoUrl = getPhotoUrl()
        Promise.all([
          privateReply(commentId, senderName),
          commentReply(commentId, senderName, photoUrl),
        ])
        .catch(console.log)
      }
      return null
    })
    res.sendStatus(200)
  }
}

export default handleWebhookEvent
