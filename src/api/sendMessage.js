import { createFetch, method, base, recv, params, json } from 'http-client'
import { PAGE_ACCESS_TOKEN } from '../config'


const parseJSON = recv(
  response =>
    response.clone().json()
    .then((jsonData) => {
      response.data = jsonData
      return response
    })
    .catch(() => response),
)

const sendMessage = messageData => createFetch(
  base('https://graph.facebook.com/v2.9/me'),
  params({ access_token: PAGE_ACCESS_TOKEN }),
  method('POST'),
  parseJSON,
  json(messageData),
)('/messages')

export default sendMessage
