import { VERIFY_TOKEN } from '../config'

const webhookVerification = (req, res) => {
  if (req.query['hub.verify_token'] === VERIFY_TOKEN) {
    res.send(req.query['hub.challenge'])
  } else {
    res.send('Error, wrong validation token')
  }
}

export default webhookVerification
