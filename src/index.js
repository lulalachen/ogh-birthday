import express from 'express'
import bodyParser from 'body-parser'
import http from 'http'
import FB from 'fb'
import api from './api'

const {
  PAGE_ACCESS_TOKEN,
  APP_NAME = 'ogh-birthday',
  PORT = 5000,
} = process.env

FB.setAccessToken(PAGE_ACCESS_TOKEN)

const app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
  extended: true,
}))

setInterval(() => {
  http.get(`https://${APP_NAME}.herokuapp.com/`, () => {})
}, (600000 * Math.random()) + 600000)

app.use('/', api)
app.listen(PORT, () => {
  console.log('App start!')
})
